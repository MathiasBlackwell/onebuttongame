
console.log("Script Loaded");

//variables pog
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

let noTimer = 1000;
let eventNum = Math.floor((Math.random() * 10) + 1);
let eventText = "";

//backbone of the game, so gameloop, draw, update.. nothing else pog.
function gameLoop()
{
    update(); 
    draw();
    requestAnimationFrame(gameLoop);    
}

function draw()
{
    
    context.clearRect(0, 0, canvas.width, canvas.height);
    //bg to timer
    context.fillStyle = '#ffffff'; 
    context.fillRect(80, 5, 1040, 30);

    //actual timer
    context.fillStyle = '#b00b69'; 
    context.fillRect(100,10, noTimer, 20);

    //temp event
    context.font = "48px serif";
    context.fillText(eventText, 150, canvas.height/2);
}



function update()
{
    timer();
    eventsList();
}

//game functions

function timer()
{
    //decremates the timer
    noTimer -= 1;

    //if the timer reaches 0, reset event and clock
    if (noTimer == 0)
        {
            noTimer = 1000;
            eventNum = Math.floor((Math.random() * 10) + 1);
        }
}

function spacebar()
{
    noTimer = 1000;
    eventNum = Math.floor((Math.random() * 10) + 1);
}

//player input, it is just one :)
let gamerInput = new GamerInput("None");

function GamerInput(input)
{
    //input is passed from input functions, left, right etc.
    this.action = input;
}

function input(event) {

    if (event.type === "keydown") {

        switch (event.keyCode) 
        {
            case 32: // Space Bar
                spacebar();
                console.log("Action");
                break;

            default:
                gamerInput = new GamerInput("None");

        }
    }
    else
    {
        gamerInput = new GamerInput("None");
    }
}


//events
function eventsList()
{
    eventText = eventNum;

    switch(eventNum)
    {
        case 1:
            eventText = "Would you like to increase military spending?";
            break;
        case 2:
            eventText = "Sell excess wheat for gold in nearby towns?"
            break;
        case 3:
            eventText = "Enact pro-life laws to increase population?"
            break;
        case 4:
            eventText = "Give all the citizen a pet cat named Milo?"
            break;
        case 5:
            eventText = "Ducks are strutting the city, should we hunt?"
            break;
        case 6:
            eventText = "??????????????????????????????????????????"
            break;
        case 7:
            eventText = "The 2018 among us game is trending, ban it?"
            break;
        case 8:
            eventText = "The mages found a precious stone, sell it?"
            break;
        case 9:
            eventText = "A circus wishes to stop in town, allow it?"
            break;
        case 10:
            eventText = "Matt is the coolest? You agree. Yes. Yes you."
            break;
    }


}


////======STart the loopo=======////
window.requestAnimationFrame(gameLoop);

window.addEventListener('keydown',input);
window.addEventListener('keyup',input);
