Lab 3:


Brief

Your problem to solve is to make a game whose interface is limited to a single button.Create the gameplay, then create all the visual and audio assets required for the game to unfold.
A button has two states: pressed and released. How can you design a core game mechanic solely using thechanges between these states? These actions could control movement (running, jumping, flying), an action(attacking, transforming), or a change in the environment (gravity, weather, friction). 
For multiplayer games,your system may use one button per player. You are encouraged to think beyond making a “runner” game—themost common one-button game mechanic, in which the player’s avatar jumps over pits and obstacles.